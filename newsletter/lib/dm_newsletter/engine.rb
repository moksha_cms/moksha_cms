require 'dm_core'
require 'gibbon'

module DmNewsletter
  class Engine < ::Rails::Engine
    isolate_namespace DmNewsletter

    initializer 'engine.assets.precompile' do |app|
      app.config.assets.precompile += %w[dm_newsletter/manifest.js]
    end
  end
end
