// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require admin_theme_2017/theme
//= require dm_core/admin
//= require dm_cms/admin
//= require dm_event/admin
//= require dm_forum/admin
//= require dm_lms/admin
//= require application_admin
