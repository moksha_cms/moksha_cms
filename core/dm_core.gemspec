require_relative 'lib/dm_core/version'

#--- Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'dm_core'
  s.version     = DmCore::VERSION
  s.authors     = ['Brett Walker']
  s.email       = ['github@digitalmoksha.com']
  s.homepage    = 'https://gitlab.com/moksha_cms/moksha_cms'
  s.licenses    = 'MIT'
  s.summary     = 'Part of MokshaCms, providing core functionality'
  s.description = 'Part of MokshaCms, providing core functionality, including internationalization'

  s.files       = Dir['{app,config,db,lib}/**/*'] + ['MIT-LICENSE', 'Rakefile', 'README.md']
  s.test_files  = Dir['spec/**/*']

  #--- don't forget to add 'require' statement in engine.rb
  s.add_dependency 'aasm', '~> 5.5'
  s.add_dependency 'actionview-encoded_mail_to', '~> 1.0'
  s.add_dependency 'activemerchant', '~> 1.126'
  s.add_dependency 'acts-as-taggable-on', '~> 10.0'
  s.add_dependency 'acts_as_votable', '~> 0.14.0'
  s.add_dependency 'amoeba', '~> 3.3.0'
  s.add_dependency 'ancestry', '~> 4.3'
  s.add_dependency 'aws-sdk-s3', '~> 1.152'
  s.add_dependency 'babosa', '~> 2.0'             # for better unicode slug handling with friendly_id
  s.add_dependency 'biggs', '~> 0.7.0'            # address formatting
  s.add_dependency 'cancancan', '~> 3.6'          # Authorization
  s.add_dependency 'carrierwave', '~> 2.2.6'      # [TODO] update to version 3
  s.add_dependency 'carrierwave-aws', '~> 1.6'
  s.add_dependency 'country_select', '~> 9.0'     # TODO: don't think needed any more.  Look into `countries` gem
  s.add_dependency 'daemons', '~> 1.4'
  s.add_dependency 'delayed_job', '~> 4.1'
  s.add_dependency 'delayed_job_active_record', '~> 4.1'
  s.add_dependency 'devise', '~> 4.9'             # Authentication
  s.add_dependency 'digitalmoksha-acts_as_commentable', '~> 6.0' # we customize our usage, so can't use the 'with_threading' version
  s.add_dependency 'dm_preferences', '~> 1.6'
  s.add_dependency 'dm_ruby_extensions', '~> 1.5'
  s.add_dependency 'exception_notification'
  s.add_dependency 'friendly_id', '~> 5.5.1'
  s.add_dependency 'globalize', '~> 6.3'
  s.add_dependency 'highline', '~> 3.0'           # Necessary for the install generator
  s.add_dependency 'kramdown', '~> 2.4'
  s.add_dependency 'liquid', '~> 5.5'
  s.add_dependency 'mini_magick', '~> 4.13'
  s.add_dependency 'money-rails', '~> 1.15'
  s.add_dependency 'offsite_payments', '~> 2.7'   #--| of PaymentHistory model
  s.add_dependency 'paper_trail', '~> 15.1'       # table versioning
  s.add_dependency 'partisan', '~> 0.5'           # acts_as_follower clone
  s.add_dependency 'rack-attack'
  s.add_dependency 'rails', '>= 7.0', '< 7.1'
  s.add_dependency 'rails-i18n', '~> 7.0'
  s.add_dependency 'ranked-model', '~> 0.4.9'     # sort order for a list
  s.add_dependency 'recaptcha', '~> 5.17'
  s.add_dependency 'RedCloth', '~> 4.3'
  s.add_dependency 'rolify', '~> 6.0'             # User Roles
  s.add_dependency 'sanitize', '~> 6.1'
  s.add_dependency 'sentry-raven', '~> 3.1'
  s.add_dependency 'simple_form', '~> 5.3'        # Form handling
  s.add_dependency 'unicode'                      # needed for babosa
  s.add_dependency 'validates_email_format_of', '~> 1.8'
  s.add_dependency 'will_paginate', '~> 4.0'      # pagination
end
