# Wrap core level specific CanCan rules.  Should be included in the main app's
# Ability class.
#------------------------------------------------------------------------------
module DmCore
  module Concerns
    module Ability
      extend ActiveSupport::Concern

      include CanCan::Ability
      include DmCms::Concerns::Ability        if defined?(DmCms)
      include DmEvent::Concerns::Ability      if defined?(DmEvent)
      include DmForum::Concerns::Ability      if defined?(DmForum)
      include DmLms::Concerns::Ability        if defined?(DmLms)
      include DmNewsletter::Concerns::Ability if defined?(DmNewsletter)

      included do
        def initialize(user)
          @user_roles = user.roles.all if user

          dm_cms_abilities(user)        if respond_to? :dm_cms_abilities
          dm_event_abilities(user)      if respond_to? :dm_event_abilities
          dm_forum_abilities(user)      if respond_to? :dm_forum_abilities
          dm_lms_abilities(user)        if respond_to? :dm_lms_abilities
          dm_newsletter_abilities(user) if respond_to? :dm_newsletter_abilities
          dm_core_abilities(user)       if respond_to? :dm_core_abilities

          additional_abilities(user)
        end

        def dm_core_abilities(user)
          if user && (user.is_admin? || user.has_role?(:manager))
            can :manage, :all
            can :access_admin, :all
          end
        end

        # The main application can override this to load their own abilities
        def additional_abilities(user); end
      end
    end
  end
end
