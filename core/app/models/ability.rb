# frozen_string_literal: true

# Application's CanCan ability class. Main application can override to
# include their own abilities
#------------------------------------------------------------------------------
class Ability
  include DmCore::Concerns::Ability
end
