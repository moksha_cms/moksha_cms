# frozen_string_literal: true

# common Devise Registration controller.  the main app can add methods
# to this class by "Reopening Existing Classes Using ActiveSupport::Concern",
# https://edgeguides.rubyonrails.org/engines.html#overriding-models-and-controllers
#------------------------------------------------------------------------------
class DmCore::RegistrationsController < Devise::RegistrationsController
  include DmCore::Concerns::RegistrationsController
end
