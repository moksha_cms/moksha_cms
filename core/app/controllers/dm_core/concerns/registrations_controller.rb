module DmCore
  module Concerns
    module RegistrationsController
      extend ActiveSupport::Concern
      include DmCore::PermittedParams

      included do
        before_action :configure_permitted_parameters, if: :devise_controller?
        before_action :check_captcha, only: [:create]
      end

      protected

      #------------------------------------------------------------------------------
      def check_captcha
        return unless Rails.application.credentials.recaptcha_secret_key
        return if verify_recaptcha(secret_key: Rails.application.credentials.recaptcha_secret_key)

        self.resource = resource_class.new sign_up_params
        resource.validate # Look for any other validation errors besides reCAPTCHA
        set_minimum_password_length

        respond_with_navigational(resource) do
          flash.discard :recaptcha_error
          render :new
        end
      end

      # hook into devise to permit our special parameters
      #------------------------------------------------------------------------------
      def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up) do |user_params|
          devise_sign_up_params(user_params)
        end
      end

      module ClassMethods
      end
    end
  end
end
