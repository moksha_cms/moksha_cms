if (Rails.env.production? || Rails.env.staging?) && Rails.application.credentials.sentry_dsn.present?
  Raven.configure do |config|
    config.dsn = Rails.application.credentials.sentry_dsn
  end
end
