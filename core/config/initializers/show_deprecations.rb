# frozen_string_literal: true

if Rails.env.development? || Rails.env.production_local?
  require 'terminal-notifier'

  ActiveSupport::Deprecation.behavior = [:stderr, :log, :notify]
  ActiveSupport::Notifications.subscribe('deprecation.rails') do |_name, _start, _finish, _id, payload|
    TerminalNotifier.notify(payload[:message], title: Rails.application.class.module_parent_name)
  end
end
