# frozen_string_literal: true

Rails.logger.info '=== Theme Initialization...'

Rails.application.reloader.to_prepare do
  DmCore::ThemeInitialization.initialize_themes
  DmCore::ThemeInitialization.load_themes
end
