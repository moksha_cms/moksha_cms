# By placing all of MokshaCms's shared dependencies in this file and then loading
# it for each component's Gemfile, we can be sure that we're only testing just
# the one component of MokshaCms.
source 'https://rubygems.org'

gem 'rails', '~> 7.0.4'

gem 'dm_preferences',         '~> 1.6.0'
gem 'themes_for_rails',       git: 'https://github.com/digitalmoksha/themes_for_rails.git'
gem 'aced_rails',             git: 'https://github.com/digitalmoksha/aced_rails.git'

gem 'puma', '~> 5.6.5'
gem 'sass-rails', '>= 6'
gem 'uglifier', '>= 4.2.0'
gem 'coffee-rails', '~> 5.0'
gem 'mini_racer', '~> 0.6.3', platforms: :ruby
gem 'bootsnap', '>= 1.13.0', require: false
gem 'webpacker', '~> 5.4'
gem 'rack-attack'

# Silence net protocol warnings
# See https://github.com/rails/rails/pull/44175
gem 'net-http'

group :development, :test do
  gem 'sqlite3', '~> 1.5.2'
  gem 'pry-byebug'

  gem 'mocha', '~> 1.15', require: false
  gem 'rspec-rails', '~> 6.0.0'
  gem 'factory_bot_rails', '~> 6.2'

  gem 'rubocop', '~> 1.36'
  gem 'rubocop-rspec', '~> 2.13'
end

group :development do
  gem 'web-console', '>= 4.2'
  gem 'listen'
  gem 'spring'
end

group :test do
  gem 'faker', '~> 2.23'
  gem 'capybara', '>= 3.37'
  gem 'database_cleaner', '~> 2.0'
  gem 'launchy', '~> 2.5'
  gem 'selenium-webdriver', '~> 4.22'
  gem 'rspec-formatter-webkit'
  gem 'rails-controller-testing'
  gem 'shoulda-matchers'
  gem 'syntax'
  gem 'webmock', '~> 3.18'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
