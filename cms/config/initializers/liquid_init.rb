# frozen_string_literal: true

# Set up handling of liquid templates
# require each tag -- registration is done in the tag file itself,
# so they can't be autoloaded
Rails.application.reloader.to_prepare do
  Dir.glob(File.expand_path('../../../app/markup/dm_cms/liquid/tags/*.rb', __FILE__)).sort.each do |path|
    require path
  end

  Dir.glob(File.join(Rails.root, '/lib/liquid/tags/*.rb')).sort.each do |path|
    require path
  end

  Dir.glob(File.expand_path('../../../app/markup/dm_cms/liquid/filters/*.rb', __FILE__)).sort.each do |path|
    require path
  end

  Dir.glob(File.join(Rails.root, '/lib/liquid/filters/*.rb')).sort.each do |path|
    require path
  end
end
