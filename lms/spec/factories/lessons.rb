FactoryBot.define do
  factory :lesson, class: Lesson do
    slug            { 'lesson-1' }
    title           { 'Lesson 1' }
    association     :course

    factory :lesson2 do
      slug          { 'lesson-2' }
      title         { 'Lesson 2' }
    end

    factory :lesson3 do
      slug          { 'lesson-3' }
      title         { 'Lesson 3' }
    end
  end
end
